

function Pokemon(name, lvl, hp){
	this.name = name;
	this.level = lvl;
	this.health = hp;
	this.attack = lvl;

	// methods
	this.tackle = function(target){
		console.log(this.name + ' '+ 'tackled ' + target.name)
	target.health = (target.health - this.attack)
		console.log(target.name + '\'s health is now reduced to ' + target.health)
	};

	this.faint = function(){
		if(this.health <= 10) {
			console.log(this.name + ' ' + 'fainted')
		}
		else {
			console.log(this.name + ' ' + 'alive and kicking')
		}
		
	};
}

// creating instance
let jigglypuff = new Pokemon('Jigglypuff', 10, 50);
let charizard = new Pokemon('Charizard', 10, 50); 

jigglypuff.tackle(charizard);
charizard.tackle(jigglypuff);
jigglypuff.tackle(charizard);
charizard.tackle(jigglypuff);
jigglypuff.tackle(charizard);
charizard.tackle(jigglypuff);
jigglypuff.tackle(charizard);
charizard.faint();



